package org.lottery;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ResultsMap {
    private Map<Date, Map<LotteryWinType, Integer>> results = new HashMap<>();

    private void addWinToResults(LotteryWinType winType, Date drawingDate) {
        Map<LotteryWinType, Integer> winMap = results.get(drawingDate);
        if (winMap == null) {
            winMap = new HashMap<>();
        }
        Integer numberOfWins = winMap.get(winType);
        if (numberOfWins == null) {
            numberOfWins = 0;
        }
        numberOfWins++;
        winMap.put(winType, numberOfWins);
        results.put(drawingDate, winMap);
    }


    public void showResults(List<Ticket> myNumbers, Map<Date, Ticket> historicalNumbers) {
        WinningAmountRules winningAmountRules = new WinningAmountRules();

        for (Ticket myTicket : myNumbers) {

            for (Ticket historicTicket : historicalNumbers.values()) {
                int winningNumberCount = checkHowManyNumbersAreCorrect(myTicket, historicTicket);
                boolean megaBallMatched = myTicket.getMegaBall() == historicTicket.getMegaBall();


                if (winningNumberCount == 5 && megaBallMatched) {
                    addWinToResults(LotteryWinType.FIVE_AND_MEGABALL, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (winningNumberCount == 5) {
                    addWinToResults(LotteryWinType.FIVE_NUMBERS, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (winningNumberCount == 4 && megaBallMatched) {
                    addWinToResults(LotteryWinType.FOUR_AND_MEGABALL, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (winningNumberCount == 4) {
                    addWinToResults(LotteryWinType.FOUR_NUMBERS, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (winningNumberCount == 3 && megaBallMatched) {
                    addWinToResults(LotteryWinType.THREE_AND_MEGABALL, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (winningNumberCount == 3) {
                    addWinToResults(LotteryWinType.THREE_NUMBERS, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (winningNumberCount == 2 && megaBallMatched) {
                    addWinToResults(LotteryWinType.TWO_NUMBERS_AND_MEGABALL, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (winningNumberCount == 1 && megaBallMatched) {
                    addWinToResults(LotteryWinType.ONE_NUMBER_AND_MEGABALL, winDateForResultsMap(historicalNumbers, historicTicket));
                } else if (megaBallMatched) {
                    addWinToResults(LotteryWinType.MEGABALL_ONLY, winDateForResultsMap(historicalNumbers, historicTicket));
                }
            }
        }

        winningAmountRules.getWinAmount(results);

    }

    private Date winDateForResultsMap(Map<Date, Ticket> map, Ticket ticketsThatMatchesDAte) {
        Date drawingDate = null;
        for (Map.Entry<Date, Ticket> entry : map.entrySet()) {
            if (entry.getValue().equals(ticketsThatMatchesDAte)) {
                drawingDate = entry.getKey();
            }
        }
        return drawingDate;
    }

    private int checkHowManyNumbersAreCorrect(Ticket myTicket, Ticket historicTicket) {
        int count = 0;

        for (Integer i : historicTicket.getBallNumbers()) {
            if (myTicket.getBallNumbers().contains(i)) {
                count++;
            }
        }
        return count;
    }
}