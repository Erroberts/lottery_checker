package org.lottery;

import java.util.Date;
import java.util.Map;

public class WinningAmountRules {


    private Date getTheWinDateForWinAmount(Map<Date, Map<LotteryWinType, Integer>> map, Map<LotteryWinType, Integer> innerMap) {
        Date drawingDate = null;
        for (Map.Entry<Date, Map<LotteryWinType, Integer>> entry : map.entrySet()) {
            if (entry.getValue().equals(innerMap)) {
                drawingDate = entry.getKey();
            }
        }
        return drawingDate;
    }


    void getWinAmount(Map<Date, Map<LotteryWinType, Integer>> map) {
        int winAmount = 0;


        for (Map<LotteryWinType, Integer> outerMapValue : map.values()) {
            //       print("This is the outer map value"+"\n"+outerMapValue);
            Date outerMapKay = getTheWinDateForWinAmount(map, outerMapValue);
            for (LotteryWinType innerMapKey : outerMapValue.keySet()) {
                //     print("this is the inner map key"+"\n"+innerMapKey);

                if (innerMapKey == LotteryWinType.FIVE_AND_MEGABALL) {
                    print("On" + map.keySet() + " you won the Jackpot Congrats");
                } else if (innerMapKey == LotteryWinType.FIVE_NUMBERS) {
                    winAmount = outerMapValue.get(innerMapKey) * 1000000;
                } else if (innerMapKey == LotteryWinType.FOUR_AND_MEGABALL) {
                    winAmount = outerMapValue.get(innerMapKey) * 10000;
                } else if (innerMapKey == LotteryWinType.FOUR_NUMBERS) {
                    winAmount = outerMapValue.get(innerMapKey) * 500;
                } else if (innerMapKey == LotteryWinType.THREE_AND_MEGABALL) {
                    winAmount = outerMapValue.get(innerMapKey) * 200;
                } else if (innerMapKey == LotteryWinType.THREE_NUMBERS) {
                    winAmount = outerMapValue.get(innerMapKey) * 10;
                } else if (innerMapKey == LotteryWinType.TWO_NUMBERS_AND_MEGABALL) {
                    winAmount = outerMapValue.get(innerMapKey) * 10;
                } else if (innerMapKey == LotteryWinType.ONE_NUMBER_AND_MEGABALL) {
                    winAmount = outerMapValue.get(innerMapKey) * 4;
                } else if (innerMapKey == LotteryWinType.MEGABALL_ONLY) {
                    winAmount = outerMapValue.get(innerMapKey) * 2;
                }
                print("on " + outerMapKay + " You have won " +outerMapValue.get(innerMapKey)+" times. For a total of : $"+ winAmount + " for the " + innerMapKey + " category" + "\n");
            }
        }
    }

    private void print(Object object) {
        System.out.println(object);
    }


}
