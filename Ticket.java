package org.lottery;

import java.util.List;

public class Ticket {
    private List<Integer> ballNumbers;
    private int megaBall;

    Ticket(List<Integer> ballNumbers, int megaBall) {
        this.ballNumbers = ballNumbers;
        this.megaBall = megaBall;
    }

    List<Integer> getBallNumbers() {
        return ballNumbers;
    }

    int getMegaBall() {
        return megaBall;
    }


    @Override
    public String toString() {
        return "Ticket{" +
                "ballNumbers=" + ballNumbers +
                ", megaBall=" + megaBall +
                '}';
    }
}