package org.lottery;


import java.util.Date;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.start();
    }


    private void start() {
        LotteryNumberFileReader test = new LotteryNumberFileReader();

        Map<Date, Ticket> pastNumbersAndDates = test.readFile("/data/LotteryPast.csv");
        TicketRepository myListOfTickets = new TicketRepository();
        ResultsMap winningResultsMap = new ResultsMap();
        winningResultsMap.showResults(myListOfTickets.getListOfTickets(10000), pastNumbersAndDates);
    }
}


