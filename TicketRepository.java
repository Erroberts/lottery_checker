package org.lottery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class TicketRepository {
    private List<Ticket> listOfTickets = new ArrayList<>();
    private Random choice = new Random();

    List<Ticket> getListOfTickets(int howMany) {
        createListOfTickets(howMany);
        return listOfTickets;
    }


    @Override
    public String toString() {
        return "TicketRepository{" +
                "listOfTickets=" + listOfTickets +
                '}';
    }

    private Ticket createTicket() {
        List<Integer> temp = new ArrayList<>();
        int megaBall = choice.nextInt(24) + 1;
        while (temp.size() < 5) {
            int number = choice.nextInt(69) + 1;
            if (!temp.contains(number)) {
                temp.add(number);
            }
        }
        Collections.sort(temp);
        return new Ticket(temp, megaBall);
    }


    private void createListOfTickets(int howMany) {
        while (listOfTickets.size() < howMany) {
            createTicket();
            if (!listOfTickets.contains(createTicket())) {
                listOfTickets.add(createTicket());
            }
        }
    }
}