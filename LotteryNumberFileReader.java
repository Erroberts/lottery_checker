package org.lottery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class LotteryNumberFileReader {

    public Map<Date, Ticket> readFile(String fileName) {

        Map<Date, Ticket> winningMap = new HashMap<>();
        try (InputStreamReader isr = new InputStreamReader(LotteryNumberFileReader.class.getResourceAsStream(fileName));
             BufferedReader reader = new BufferedReader(isr)) {

            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

            boolean firstLine = true;
            String line = reader.readLine();
            while (line != null) {
                if (firstLine) {
                    firstLine = false;
                    line = reader.readLine();
                    continue;
                }
                String[] data = line.split(",");
                Date date = formatter.parse(data[0]);
                String winningNumbers = data[1];
                String[] numbers = winningNumbers.split("\\s");
                List<Integer> listOfWinningNumbers = new ArrayList<>();
                
                for (String i: numbers){
                    listOfWinningNumbers.add(Integer.valueOf(i));
                    
                }
                 int megaBall = Integer.valueOf(data[2]);
            
                Collections.sort(listOfWinningNumbers);
                Ticket winningTicket = new Ticket(listOfWinningNumbers, megaBall);


                winningMap.put(date, winningTicket);
                line = reader.readLine();

            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return winningMap;
    }
}